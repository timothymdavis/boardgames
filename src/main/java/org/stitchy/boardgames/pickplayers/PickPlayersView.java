package org.stitchy.boardgames.pickplayers;

import org.stitchy.common.view.StitchyView;

/**
 * The view methods specific to picking a game.
 */
public interface PickPlayersView extends StitchyView {
}
