package org.stitchy.boardgames.pickplayers;

/**
 * The default implementation of the {@link PickPlayersController} interface.
 */
public class PickPlayersControllerDefault implements PickPlayersController {

    private PickPlayersView view;

    /**
     * Fires when the user has selected one player.
     */
    @Override
    public void onPickOnePlayer() {
    }

    /**
     * Fires when the user has selected two players.
     */
    @Override
    public void onPickTwoPlayers() {
    }

    @Override
    public void onCreate(PickPlayersView view) {
        this.view = view;
    }

}

