package org.stitchy.boardgames.pickplayers;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.google.inject.Inject;
import org.stitchy.boardgames.R;
import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;

public class PickPlayersViewUgly extends RoboActivity implements PickPlayersView {

    @Inject
    protected PickPlayersController controller;

    @InjectView(R.id.pickOnePlayer)
    protected Button pickOnePlayer;

    @InjectView(R.id.pickTwoPlayers)
    protected Button pickTwoPlayers;

    private void navigateToPickPlayers(Class aClass) {
        startActivity(new Intent(this, aClass));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.pick_players);

        controller.onCreate(this);

        pickTwoPlayers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToPickPlayers(PickPlayersViewUgly.class);
            }
        });

        pickOnePlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToPickPlayers(PickPlayersViewUgly.class);
            }
        });
    }

}

