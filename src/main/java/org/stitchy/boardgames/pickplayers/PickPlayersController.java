package org.stitchy.boardgames.pickplayers;

import org.stitchy.common.controller.StitchyController;

/**
 * The pick players controller operations.
 */
public interface PickPlayersController extends StitchyController<PickPlayersView> {

    /**
     * Fires when the user has selected one player.
     */
    void onPickOnePlayer();

    /**
     * Fires when the user has selected two players.
     */
    void onPickTwoPlayers();

}
