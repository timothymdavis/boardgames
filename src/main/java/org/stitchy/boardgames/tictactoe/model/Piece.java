package org.stitchy.boardgames.tictactoe.model;

/**
 * The pieces that can be placed on the board.
 */
public enum Piece {

    /**
     * Represents an X on the tic-tac-toe board.
     */
    X,

    /**
     * Represents an O on the tic-tac-toe board.
     */
    O

}
