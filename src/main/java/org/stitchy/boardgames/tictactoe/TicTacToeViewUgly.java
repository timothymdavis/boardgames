package org.stitchy.boardgames.tictactoe;

import java.util.HashMap;
import java.util.Map;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.google.inject.Inject;
import org.stitchy.boardgames.R;
import org.stitchy.boardgames.tictactoe.model.Piece;
import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;

/**
 * A somewhat ugly tic-tac-toe view implementation. It's super simple though, and it gets the job done.
 */
public class TicTacToeViewUgly extends RoboActivity implements TicTacToeView {

    private Map<Integer, Button> buttonMap;

    @InjectView(R.id.buttonNewGame)
    protected Button buttonNewGame;

    @InjectView(R.id.buttonX0Y0)
    protected Button buttonX0Y0;

    @InjectView(R.id.buttonX0Y1)
    protected Button buttonX0Y1;

    @InjectView(R.id.buttonX0Y2)
    protected Button buttonX0Y2;

    @InjectView(R.id.buttonX1Y0)
    protected Button buttonX1Y0;

    @InjectView(R.id.buttonX1Y1)
    protected Button buttonX1Y1;

    @InjectView(R.id.buttonX1Y2)
    protected Button buttonX1Y2;

    @InjectView(R.id.buttonX2Y0)
    protected Button buttonX2Y0;

    @InjectView(R.id.buttonX2Y1)
    protected Button buttonX2Y1;

    @InjectView(R.id.buttonX2Y2)
    protected Button buttonX2Y2;

    @Inject
    protected TicTacToeController controller;

    @InjectView(R.id.textView)
    protected TextView textView;

    private void onButtonClicked(int x, int y) {
        controller.onUserMove(x, y);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.tic_tac_toe_ugly);

        controller.onCreate(this);

        buttonNewGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonClicked(-1, -1);
            }
        });

        buttonMap = new HashMap<Integer, Button>();
        buttonMap.put(0, buttonX0Y0);
        buttonMap.put(1, buttonX0Y1);
        buttonMap.put(2, buttonX0Y2);
        buttonMap.put(10, buttonX1Y0);
        buttonMap.put(11, buttonX1Y1);
        buttonMap.put(12, buttonX1Y2);
        buttonMap.put(20, buttonX2Y0);
        buttonMap.put(21, buttonX2Y1);
        buttonMap.put(22, buttonX2Y2);

        for (int key : buttonMap.keySet()) {
            Button button = buttonMap.get(key);

            final int x = key / 10;
            final int y = key % 10;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onButtonClicked(x, y);
                }
            });
        }
    }

    @Override
    public void displayNewGameButton(boolean isVisible) {
        buttonNewGame.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void displayText(String text) {
        textView.setText(text);
    }

    @Override
    public void setController(TicTacToeController controller) {
        this.controller = controller;
        controller.onCreate(this);
    }

    @Override
    public void setPiece(Piece piece, int x, int y) {
        int key = x * 10 + y;
        Button button = buttonMap.get(key);
        button.setText(piece == null ? "" : piece.name());
    }
}

