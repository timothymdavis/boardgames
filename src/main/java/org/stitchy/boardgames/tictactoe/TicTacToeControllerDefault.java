package org.stitchy.boardgames.tictactoe;

import java.util.Random;

import org.stitchy.boardgames.tictactoe.model.Board;
import org.stitchy.boardgames.tictactoe.model.Piece;

/**
 * The default implementation of the {@link TicTacToeController} interface.
 */
public class TicTacToeControllerDefault implements TicTacToeController {

    private final Board board;
    private final Random random;
    private TicTacToeView view;

    /**
     * The constructor for this class.
     */
    public TicTacToeControllerDefault() {
        board = new Board();
        random = new Random();
    }

    private void computerMove() {
        while (Piece.O != board.getLastPiece()) {
            onMove(Piece.O, random.nextInt(3), random.nextInt(3));
        }
    }

    @Override
    public void onCreate(TicTacToeView view) {
        this.view = view;
    }

    private void onMove(Piece piece, int x, int y) {
        if (board.isWithinBoundaries(x, y)) {
            if (!board.isPieceSet(x, y)) {
                board.setPiece(piece, x, y);
                view.setPiece(piece, x, y);
                if (board.hasWinner(x, y)) {
                    view.setController(new TicTacToeControllerGameOver(piece));
                }
                else if (!board.hasMovesRemaining()) {
                    view.setController(new TicTacToeControllerGameOver());
                }
                else {
                    computerMove();
                }
            }
        }
    }

    @Override
    public void onUserMove(int x, int y) {
        onMove(Piece.X, x, y);
    }

}
