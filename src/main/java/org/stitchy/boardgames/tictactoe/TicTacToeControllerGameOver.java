package org.stitchy.boardgames.tictactoe;

import org.stitchy.boardgames.tictactoe.model.Piece;

/**
 * The controller that handles the game over state.
 */
public class TicTacToeControllerGameOver implements TicTacToeController {

    private TicTacToeView view;
    private Piece winningPiece;

    /**
     * The constructor to use when the game is a draw.
     */
    public TicTacToeControllerGameOver() {
    }

    /**
     * The constructor for this class.
     *
     * @param winningPiece The last piece played.
     */
    public TicTacToeControllerGameOver(Piece winningPiece) {
        this.winningPiece = winningPiece;
    }

    private void clearExistingGame() {
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                view.setPiece(null, x, y);
            }
        }
    }

    @Override
    public void onCreate(TicTacToeView view) {
        this.view = view;
        view.displayNewGameButton(true);
        view.displayText(winningPiece == null
                ? "The game was a Draw."
                : String.format("Player %s won!", winningPiece.name()));
    }

    @Override
    public void onUserMove(int x, int y) {
        if (x == -1 && y == -1) {
            clearExistingGame();
            view.displayNewGameButton(false);
            view.displayText("");
            view.setController(new TicTacToeControllerDefault());
        }
    }

}
