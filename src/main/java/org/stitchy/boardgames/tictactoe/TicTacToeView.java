package org.stitchy.boardgames.tictactoe;

import org.stitchy.boardgames.tictactoe.model.Piece;
import org.stitchy.common.view.StitchyView;

/**
 * The view methods specific to the tic-tac-toe game.
 */
public interface TicTacToeView extends StitchyView {
    /**
     * Allows the {@link TicTacToeController} object to set the new-game button as (in)visible.
     *
     * @param isVisible The flag that indicates if the the button is visible.
     */
    void displayNewGameButton(boolean isVisible);

    /**
     * DRY.
     *
     * @param text The text to display to the user.
     */
    void displayText(String text);

    /**
     * DRY.
     *
     * @param controller A reference to the controller for callbacks/state-changes.
     */
    void setController(TicTacToeController controller);

    /**
     * Sets the piece in the view.
     *
     * @param piece The piece to place (i.e. {@link Piece#X} {@link Piece#O}.)
     * @param x     The x-coordinate for the piece.
     * @param y     The y-coordinate for the piece.
     */
    void setPiece(Piece piece, int x, int y);
}
