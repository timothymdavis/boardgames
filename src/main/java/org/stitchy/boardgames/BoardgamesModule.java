package org.stitchy.boardgames;

import com.google.inject.AbstractModule;
import org.stitchy.boardgames.pickgame.PickGameController;
import org.stitchy.boardgames.pickgame.PickGameControllerDefault;
import org.stitchy.boardgames.pickplayers.PickPlayersController;
import org.stitchy.boardgames.pickplayers.PickPlayersControllerDefault;
import org.stitchy.boardgames.tictacnine.TicTacNineController;
import org.stitchy.boardgames.tictacnine.TicTacNineControllerDefault;
import org.stitchy.boardgames.tictactoe.TicTacToeController;
import org.stitchy.boardgames.tictactoe.TicTacToeControllerDefault;

/**
 * The Robo Guice module that handles dependency injection configuration.
 */
public class BoardgamesModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(PickGameController.class).to(PickGameControllerDefault.class);
        bind(PickPlayersController.class).to(PickPlayersControllerDefault.class);
        bind(TicTacToeController.class).to(TicTacToeControllerDefault.class);
        bind(TicTacNineController.class).to(TicTacNineControllerDefault.class);
    }

}
