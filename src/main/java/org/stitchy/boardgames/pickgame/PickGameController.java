package org.stitchy.boardgames.pickgame;

import org.stitchy.common.controller.StitchyController;

/**
 * The pick game controller operations.
 */
public interface PickGameController extends StitchyController<PickGameView> {

    /**
     * Fires when the user has selected the game tic-tac-toe.
     */
    void onPickTicTacToe();

    /**
     * Fires when the user has selected the game tic-tac-nine.
     */
    void onPickTicTacNine();

}
