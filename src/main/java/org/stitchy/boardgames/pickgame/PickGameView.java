package org.stitchy.boardgames.pickgame;

import org.stitchy.common.view.StitchyView;

/**
 * The view methods specific to picking a game.
 */
public interface PickGameView extends StitchyView {
}
