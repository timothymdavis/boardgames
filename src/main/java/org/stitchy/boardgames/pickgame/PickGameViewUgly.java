package org.stitchy.boardgames.pickgame;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.google.inject.Inject;
import org.stitchy.boardgames.R;
import org.stitchy.boardgames.tictacnine.TicTacNineViewUgly;
import org.stitchy.boardgames.tictactoe.TicTacToeViewUgly;
import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;

public class PickGameViewUgly extends RoboActivity implements PickGameView {

    @InjectView(R.id.pickTicTacToe)
    protected Button pickTicTacToe;

    @InjectView(R.id.pickTicTacNine)
    protected Button pickTicTacNine;

    @Inject
    protected PickGameController controller;

    private void navigateToGame(Class aClass) {
        startActivity(new Intent(this, aClass));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.pick_game);

        controller.onCreate(this);

        pickTicTacToe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToGame(TicTacToeViewUgly.class);
            }
        });

        pickTicTacNine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToGame(TicTacNineViewUgly.class);
            }
        });
    }

}

