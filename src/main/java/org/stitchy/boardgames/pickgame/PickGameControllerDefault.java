package org.stitchy.boardgames.pickgame;

/**
 * The default implementation of the {@link PickGameController} interface.
 */
public class PickGameControllerDefault implements PickGameController {

    private PickGameView view;

    @Override
    public void onCreate(PickGameView view) {
        this.view = view;
    }

    @Override
    public void onPickTicTacToe() {
    }

    @Override
    public void onPickTicTacNine() {
    }

}
