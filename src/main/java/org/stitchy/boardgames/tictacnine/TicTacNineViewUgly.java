package org.stitchy.boardgames.tictacnine;

import android.os.Bundle;
import com.google.inject.Inject;
import org.stitchy.boardgames.R;
import roboguice.activity.RoboActivity;

/**
 * A somewhat ugly tic-tac-toe view implementation. It's super simple though, and it gets the job done.
 */
public class TicTacNineViewUgly extends RoboActivity implements TicTacNineView {

    @Inject
    protected TicTacNineController controller;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.tic_tac_nine_ugly);

        controller.onCreate(this);
    }

}

