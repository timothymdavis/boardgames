package org.stitchy.boardgames.tictacnine;

import org.stitchy.common.controller.StitchyController;

/**
 * The tic-tac-toe controller operations.
 */
public interface TicTacNineController extends StitchyController<TicTacNineView> {

    /**
     * Fires when the user has selected a move. This move might be invalid. The controller will validate it. If it is
     * invalid then nothing will happen.
     *
     * @param x The x-coordinate chosen by the user.
     * @param y The y-coordinate chosen by the user.
     */
    void onUserMove(int x, int y);

}
