package org.stitchy.boardgames.tictacnine.model;

/**
 * The board that the tic-tac-toe game is played on.
 */
public class Board {

    private final Piece[][] board;
    private Piece lastPiece;
    private int pieceCount;

    /**
     * The constructor for this class.
     */
    public Board() {
        board = new Piece[3][3];
    }

    /**
     * DRY.
     *
     * @return Gets the last piece that was played.
     */
    public Piece getLastPiece() {
        return lastPiece;
    }

    /**
     * DRY.
     *
     * @return A boolean indicating if there are moves remaining on the board.
     */
    public boolean hasMovesRemaining() {
        return pieceCount < 9;
    }

    /**
     * DRY.
     *
     * @param x The x-coordinate to check.
     * @param y The y-coordinate to check.
     * @return A boolean indicating if the game is won.
     */
    public boolean hasWinner(int x, int y) {
        int horizontal = 0;
        int vertical = 0;
        int diagonalOne = 0;
        int diagonalTwo = 0;

        for (int i = 0; i < 3; i++) {
            horizontal += board[i][y] == Piece.X ? 1 : board[i][y] == Piece.O ? 4 : 0;
            vertical += board[x][i] == Piece.X ? 1 : board[x][i] == Piece.O ? 4 : 0;
            diagonalOne += board[i][i] == Piece.X ? 1 : board[i][i] == Piece.O ? 4 : 0;
            diagonalTwo += board[2 - i][i] == Piece.X ? 1 : board[2 - i][i] == Piece.O ? 4 : 0;
        }

        return horizontal == 12
                || vertical == 12
                || diagonalOne == 12
                || diagonalTwo == 12
                || horizontal == 3
                || vertical == 3
                || diagonalOne == 3
                || diagonalTwo == 3;
    }

    /**
     * DRY.
     *
     * @param x The x-coordinate to check.
     * @param y The y-coordinate to check.
     * @return A boolean indicating if a piece has already been placed at x and y.
     */
    public boolean isPieceSet(int x, int y) {
        return board[x][y] != null;
    }

    /**
     * DRY.
     *
     * @param x The x-coordinate to check.
     * @param y The y-coordinate to check.
     * @return A flag indicating if the x and y coordinates are within the game boundaries.
     */
    public boolean isWithinBoundaries(int x, int y) {
        return x >= 0 && y >= 0 && x < board.length && y < board[x].length;
    }

    /**
     * Sets the piece at the coordinates provided. This function will throw an exception if the piece has already
     * been set.
     *
     * @param piece The piece to set (i.e. {@link Piece#X} and {@link Piece#O}.)
     * @param x     The x-coordinate where the piece will be set.
     * @param y     The y-coordinate where the piece will be set.
     * @throws IllegalStateException when the piece has already been set.
     */
    public void setPiece(Piece piece, int x, int y) {
        if (isPieceSet(x, y)) {
            throw new IllegalStateException(String.format("The piece at x=%d and y=%d has already been set.", x, y));
        }
        board[x][y] = piece;
        pieceCount++;
        lastPiece = piece;
    }
}
