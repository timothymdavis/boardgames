package org.stitchy.boardgames.tictacnine;

import org.stitchy.boardgames.tictacnine.model.Piece;

/**
 * The controller that handles the game over state.
 */
public class TicTacNineControllerGameOver implements TicTacNineController
{

    private TicTacNineView view;
    private Piece winningPiece;

    /**
     * The constructor to use when the game is a draw.
     */
    public TicTacNineControllerGameOver() {
    }

    /**
     * The constructor for this class.
     *
     * @param winningPiece The last piece played.
     */
    public TicTacNineControllerGameOver(Piece winningPiece) {
        this.winningPiece = winningPiece;
    }

    private void clearExistingGame() {
    }

    @Override
    public void onCreate(TicTacNineView view) {
        this.view = view;
    }

    @Override
    public void onUserMove(int x, int y) {
    }

}
