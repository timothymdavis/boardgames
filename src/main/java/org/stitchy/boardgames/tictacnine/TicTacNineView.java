package org.stitchy.boardgames.tictacnine;

import org.stitchy.boardgames.tictacnine.model.Piece;
import org.stitchy.common.view.StitchyView;

/**
 * The view methods specific to the tic-tac-toe game.
 */
public interface TicTacNineView extends StitchyView {
}
