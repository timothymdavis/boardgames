package org.stitchy.boardgames.tictacnine;

import java.util.Random;

import org.stitchy.boardgames.tictacnine.model.Board;

/**
 * The default implementation of the {@link TicTacNineController} interface.
 */
public class TicTacNineControllerDefault implements TicTacNineController
{

    private final Board board;
    private final Random random;
    private TicTacNineView view;

    /**
     * The constructor for this class.
     */
    public TicTacNineControllerDefault() {
        board = new Board();
        random = new Random();
    }

    @Override
    public void onCreate(TicTacNineView view) {
        this.view = view;
    }

    @Override
    public void onUserMove(int x, int y) {
    }

}
