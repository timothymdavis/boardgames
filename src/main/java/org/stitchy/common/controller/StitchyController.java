package org.stitchy.common.controller;

import org.stitchy.common.view.StitchyView;

/**
 * Interface for all controllers.
 */
public interface StitchyController<T extends StitchyView>
{
    /**
     * This method fires when the {@link StitchyView} object is created.
     *
     * @param view The {@link StitchyView} that was just created.
     */
    void onCreate(T view);
}
