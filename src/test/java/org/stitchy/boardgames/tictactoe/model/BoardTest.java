package org.stitchy.boardgames.tictactoe.model;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

public class BoardTest {
    
    @Test
    public void testGetLastPieceIsNull() throws Exception {
        Board board = new Board();
        assertThat(board.getLastPiece(), nullValue());
    }

    @Test
    public void testGetLastPieceIsO() throws Exception {
        Board board = new Board();
        board.setPiece(Piece.O, 0, 0);
        assertThat(board.getLastPiece(), is(Piece.O));
    }

    @Test
    public void testGetLastPieceIsX() throws Exception {
        Board board = new Board();
        board.setPiece(Piece.X, 0, 0);
        assertThat(board.getLastPiece(), is(Piece.X));
    }

    @Test
    public void testGetLastPieceIsXForTwoMoves() throws Exception {
        Board board = new Board();
        board.setPiece(Piece.O, 0, 1);
        board.setPiece(Piece.X, 0, 0);
        assertThat(board.getLastPiece(), is(Piece.X));
    }

    @Test
    public void testHasMovesRemainingIsFalse() throws Exception {
        Board board = new Board();
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                board.setPiece(Piece.X, x, y);
            }
        }
        assertThat(board.hasMovesRemaining(), is(false));
    }

    @Test
    public void testHasMovesRemainingIsTrue() throws Exception {
        Board board = new Board();
        assertThat(board.hasMovesRemaining(), is(true));
    }

    @Test
    public void testHasWinnerDiagonals() throws Exception {
        Board board0 = new Board();
        assertThat(board0.hasWinner(0, 1), is(false));
        for (int i = 0; i < 3; i++) {
            board0.setPiece(Piece.X, i, i);
        }
        assertThat(board0.hasWinner(0, 1), is(true));

        Board board1 = new Board();
        assertThat(board1.hasWinner(0, 1), is(false));
        for (int i = 2; i >= 0; i--) {
            board1.setPiece(Piece.X, i, i);
        }
        assertThat(board1.hasWinner(0, 1), is(true));
    }

    @Test
    public void testHasWinnerHorizontal() throws Exception {
        for (int y = 0; y < 3; y++) {
            Board board = new Board();
            for (int x = 0; x < 3; x++) {
                board.setPiece(Piece.X, x, y);
            }
            assertThat(board.hasWinner(0, y), is(true));
        }
    }

    @Test
    public void testHasWinnerVertical() throws Exception {
        for (int x = 0; x < 3; x++) {
            Board board = new Board();
            for (int y = 0; y < 3; y++) {
                board.setPiece(Piece.X, x, y);
            }
            assertThat(board.hasWinner(x, 0), is(true));
        }
    }

    @Test
    public void testHasWinnerZeroMoves() throws Exception {
        Board board = new Board();
        assertThat(board.hasWinner(0, 0), is(false));
    }

    @Test
    public void testIsPieceSetIsFalse() throws Exception {
        Board board = new Board();
        assertThat(board.isPieceSet(0, 0), is(false));
    }

    @Test
    public void testIsPieceSetIsTrue() throws Exception {
        Board board = new Board();
        board.setPiece(Piece.X, 0, 0);
        assertThat(board.isPieceSet(0, 0), is(true));
    }

    @Test
    public void testIsWithinBoundaries() throws Exception {
        Board board = new Board();
        assertThat(board.isWithinBoundaries(-1, -1), is(false));
        assertThat(board.isWithinBoundaries(-1, 0), is(false));
        assertThat(board.isWithinBoundaries(0, -1), is(false));
        assertThat(board.isWithinBoundaries(3, 3), is(false));
        assertThat(board.isWithinBoundaries(3, 0), is(false));
        assertThat(board.isWithinBoundaries(0, 3), is(false));
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                assertThat(board.isWithinBoundaries(x, y), is(true));
            }
        }
    }

    @Test
    public void testSetPiece() throws Exception {
        Board board = new Board();
        board.setPiece(Piece.X, 0, 0);
        assertThat(board.isPieceSet(0, 0), is(true));
    }

    @Test(expected = IllegalStateException.class)
    public void testSetPieceThrowsIllegalStateException() throws Exception {
        Board board = new Board();
        board.setPiece(Piece.X, 0, 0);
        board.setPiece(Piece.X, 0, 0);
    }
}
